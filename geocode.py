#### Geocoding using Google Maps API
#### aim: names -> coordinates

## read names from CSV file
# -*- coding: utf-8 -*-
import csv
with open('names.csv', 'r', encoding='utf-8') as f:
    reader = csv.reader(f)
    names = list(reader)

## prepare API
import googlemaps
from datetime import datetime
gmaps = googlemaps.Client(key='AIzaSyDmSmgxZzvcoZ6snrSodp9JiHl-p18Cads')

## convert to coordinates and save them in CSV file
with open('coordinates.csv', 'w', encoding='utf-8') as file:
    i = 0
    while i < len(names)-1:  
        geocode_result = gmaps.geocode(names[i+1][1] + ', Morocco')
        line = []
        if len(geocode_result) < 1:
            line = names[i+1][0] + ';' + ';' + ';' + ';' + '\n'
        else:

            location = geocode_result[0]['formatted_address']
            lat = geocode_result[0]['geometry']['location']['lat']
            lng = geocode_result[0]['geometry']['location']['lng']
            line = names[i+1][0] + ';' + ';' + location + ';' + str(lat) + ';' + str(lng) + '\n'
        print(line)    
        file.write(line)
        i += 1